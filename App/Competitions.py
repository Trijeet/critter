#!/usr/bin/env python

##Imports -- Py
import time
from Enemies import Enemy
import random

class Competition(object):
    '''The explore mini-game'''
    def __init__(self, Critter, Difficulty, Player):
        super(Competition, self).__init__()
        self.t = 30
        self.critter = Critter
        self.difficulty = Difficulty
        self.player = Player
        self.tgold = 30
        self.mgold = 100
        self.comp_experience = 0
        self.enemy_counter = 0
        self.generateEnemy()
        self.crit = 1


    def competitionFinish(self):
        self.goldperhp()
        self.reward()
        self.victorymsg = "You received", self.tgold + self.mgold, "gold and", str(self.comp_experience), "experience!"
        self.victory = str(self.victorymsg)
        return

    def competitionFinish2(self):
        self.goldperhp()
        self.defeatReward()
        self.defeatmsg = "You received", self.mgold, "gold for your efforts!"
        self.defeat = str(self.defeatmsg)
        return

    def generateEnemy(self):
        '''Makes an enemy out of the chosen difficulty'''
        #print(self.critter.base_hp)
        self.Enemy = Enemy(self.critter, self.difficulty)
        return

    def Enemyattack(self):
        #self.Critter.hp = self.Critter.hp
        if self.critter.hp > 0:
            self.critter.hp = self.critter.hp - self.Enemy.attack
            print(self.critter.hp, 'HP remaining')
    
    def critterAttack(self):
        self.crit = random.randint(1,20)
        if self.crit == 1:
            self.Enemy.hp -= self.critter.atk*6
            print(self.Enemy.hp)
            if self.Enemy.hp <= 0:
                self.competitionFinish()
                return False
        else:
            self.Enemy.hp -= self.critter.atk
            print(self.Enemy.hp)
            if self.Enemy.hp <= 0:
                self.competitionFinish()
                return False
        pass

    def goldperhp(self):
        '''Depending on the hppercent and the difficulty gold reward will be determined'''
        #print(self.Enemy.hp, self.Enemy.basehp)
        hppercent = 100 * int(self.Enemy.hp)/int(self.Enemy.basehp)
        if self.difficulty == 0:
            if hppercent >= 75:
                self.mgold = 3
            if hppercent <= 75:
                self.mgold = 6
            if hppercent <= 50:
                self.mgold = 9
            if hppercent <= 25:
                self.mgold = 12
            if hppercent <= 0:
                self.mgold = 20
            #goldperhp.gold = self.mgold
        if self.difficulty == 1:
            if hppercent >= 75:
                self.mgold = 6
            if hppercent <= 75:
                self.mgold = 10
            if hppercent <= 50:
                self.mgold = 14
            if hppercent <= 25:
                self.mgold = 18
            if hppercent <= 0:
                self.mgold = 30
            #goldperhp.gold = self.mgold
        if self.difficulty == 2:
            if hppercent >= 75:
                self.mgold = 10
            if hppercent <= 75:
                self.mgold = 15
            if hppercent <= 50:
                self.mgold = 20
            if hppercent <= 25:
                self.mgold = 25
            if hppercent <= 0:
                self.mgold = 50
            #goldperhp.gold = self.mgold
        

    def reward(self):    
        """updates inventory gold"""
        #print(self.critter.level, self.player.gold)
        if self.difficulty == 0:
            self.comp_experience += 500 + self.critter.level * 50
        if self.difficulty == 1:
            self.comp_experience += 800  + self.critter.level * 70
        if self.difficulty == 2:
            self.comp_experience += 1200  + self.critter.level * 120
        self.player.changeGold(amount=self.tgold+self.mgold)
        self.critter.earnExperience(amount=self.comp_experience)
    
    def defeatReward(self):
        self.player.changeGold(amount=self.mgold)

        
    def countdown(self):
        """The timer counts down from 60 per second
    on the top of the screen gain gold depending on
    how much time is remaining"""
        if self.t >= 0:
            print(self.t)
            self.t -= 1
            if self.tgold > 0:
                self.tgold -= 2
