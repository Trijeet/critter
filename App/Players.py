#!/usr/bin/env python

from Items import CannedFood, Beans, SpaghettiDinner, LowLevelElixer, MidLevelElixer, HighLevelElixer, SmallWeapon, GeniusStrategy, BearArms, Bandaids, SuitOfArmor, SpontaneousRegenerationAbility


class Player(object):
    """docstring for player"""
    def __init__(self, critter, *args, **kwargs):
        super(Player, self).__init__()
        self.critter = critter
        self.inventory = {
        "0":["cannedFood", CannedFood, 0],
        "1":["beans", Beans, 0],
        "2":["spaghettiDinner", SpaghettiDinner, 0],
        "3":["lowLevelElixer", LowLevelElixer, 0],
        "4":["midLevelElixer", MidLevelElixer, 0],
        "5":["highLevelElixer", HighLevelElixer, 0],
        "6":["smallWeapon", SmallWeapon, 0],
        "7":["geniusStrategy", GeniusStrategy, 0],
        "8":["bearArms", BearArms, 0],
        "9":["bandaids", Bandaids, 0],
        "10":["suitOfArmor", SuitOfArmor, 0],
        "11":["spontaneousRegenerationAbility", SpontaneousRegenerationAbility, 0]
        }
        self.gold = 100
        if "inventory" in kwargs.keys():
            for k, v in kwargs["inventory"].items():
                self.inventory.get(str(k), 0)[-1] += int(v[-1])
            kwargs.pop("inventory", None)

        self.__dict__.update(**kwargs)

    def changeGold(self, amount):
        self.gold += amount
        #print(self.gold, "Gold")

    def checkPurchasingPower(self, ItemId):
        if self.gold>=self.inventory[ItemId][1].value:
            self.addItem(ItemId)
            self.changeGold(amount=-self.inventory[ItemId][1].value)
            return 0
        else:
            print("You're too poor. :(")
            return -1

    def addItem(self, ItemId):
        self.inventory[ItemId][-1] += 1

        return

    # def getInventory(self,):
    #     Items = dict()
    #     for i in self.inventory:
    #       Items[i] = Items.get(i, 0) + 1
    #     return Items

    def useItem(self, ItemId):
        
        if self.inventory[ItemId][-1] < 1:
            print("You don't have this item.")
        else:
            self.removeItem(ItemId)
            self.critter.tmp_hp += self.inventory[ItemId][1].tmp_hp
            self.critter.tmp_atk += self.inventory[ItemId][1].tmp_atk
            self.critter.mod_atk += self.inventory[ItemId][1].mod_atk
            self.critter.mod_hp += self.inventory[ItemId][1].mod_hp
            self.critter.updateStats()

        return

    def removeItem(self, ItemId):
        self.inventory[ItemId][-1] -= 1

    def getGold(self):
        print(self.gold)
        return self.gold
