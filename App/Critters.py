#!/usr/bin/env python

##Imports -- py
import os

'''Critter Creatures to be cared for'''
class Critter(object):
    def __init__(self, *args, **kwargs):
        super(Critter, self).__init__()
        self.name = "Name" 
        self.level = 1
        self.experience = 0
        #All together
        self.hp = 0 
        self.atk = 0
        #Leveled Stats
        self.base_hp = 0
        self.base_atk = 0
        #Temporary Stats
        self.tmp_atk = 0
        self.tmp_hp = 0
        #Permanent Stat Boosts
        self.mod_hp = 0
        self.mod_atk = 0
        #########
        self.growths = {
                    "hp": 0,
                    "atk": 0,
        }
        self.evolution_tier = 1
        self.imgpath = ""
        self.__dict__.update(**kwargs) 

    def levelCheck(self):
        if self.experience >= self.level*1000:
            self.experience = self.experience - self.level*1000
            self.level = self.level + 1
            self.base_hp += self.growths["hp"]
            self.base_atk += self.growths["atk"]

        if self.level == 10 and self.evolution_tier == 1:
            self.evolve()
        if self.level == 25 and self.evolution_tier == 2:
            self.evolve()
        if self.level == 1000000000000000:
            print("Thank you, but... you need a life.")
            self.level = 1
            self.evolution_tier = 0
            self.evolve()

    def earnExperience(self, amount):
        self.experience += amount
        self.levelCheck()

    # Stats
    def updateStats(self):
        ##Function to update critter stats.
        self.levelCheck()
        self.hp = self.base_hp + self.mod_hp + self.tmp_hp
        self.atk = self.base_atk + self.mod_atk + self.tmp_atk
        return 0

    def competitionEnd(self):
        ##Function to reset temporary item bonuses.
        self.tmp_hp = 0
        self.tmp_atk = 0
        self.updateStats()
        return

    def getImagepath(self):
        return self.imgpath

    def getName(self):
        return self.name

    def setName(self, new_name):
        self.name = new_name

    def evolve(self):
        self.evolution_tier += 1
        new_path = self.imgpath.replace(str(self.evolution_tier-1), str(self.evolution_tier))
        if os.path.isfile(new_path):
            self.imgpath = new_path
        else:
            print("Invalid path. File path not updated.")
            self.imgpath = new_path

        if self.evolution_tier == 2 and self.breed == "Fox":
            self.mod_hp += 40
            self.mod_atk += 12
        if self.evolution_tier == 2 and self.breed == "Snake":
            self.mod_hp += 12
            self.mod_atk += 40
        if self.evolution_tier == 2 and self.breed == "Bird":
            self.mod_hp += 20
            self.mod_atk += 20
        if self.evolution_tier == 3 and self.breed == "Fox":
            self.mod_hp += 50
            self.mod_atk += 22
        if self.evolution_tier == 3 and self.breed == "Snake":
            self.mod_hp += 22
            self.mod_atk += 50
        if self.evolution_tier == 3 and self.breed == "Bird":
            self.mod_hp += 25
            self.mod_atk += 25
        return -1

'''The Critters'''
class Foxy(Critter):
    def __init__(self, *args, **kwargs):
        super(Foxy, self).__init__(*args, **kwargs)
        self.base_hp = 34
        self.base_atk = 10
        self.growths = {
                    "hp": 34,
                    "atk": 10,
        }
        self.updateStats()
        self.imgpath = "../Assets/Images/Critters/Foxy1.png"
        self.breed = "Fox"
        
        self.__dict__.update(**kwargs)

class Snek(Critter):
    def __init__(self, *args, **kwargs):
        super(Snek, self).__init__(*args, **kwargs)
        self.base_hp = 30
        self.base_atk = 14
        self.growths = {
                    "hp": 30,
                    "atk": 14,
        }
        self.updateStats()
        self.imgpath = "../Assets/Images/Critters/Snek1.png"
        self.breed = "Snake"
        self.__dict__.update(**kwargs)

class Birb(Critter):
    def __init__(self, *args, **kwargs):
        super(Birb, self).__init__(*args, **kwargs)
        self.base_hp = 32
        self.base_atk = 12
        self.growths = {
                    "hp": 32,
                    "atk": 12,
        }
        self.updateStats()
        self.imgpath = "../Assets/Images/Critters/Birb1.png"
        self.breed = "Bird"
        self.__dict__.update(**kwargs)
